const { schema } = require("./schema")

async function ensureSchemaExists(){
    await schema.ensureExists();
}

exports.init = async function(){
    await ensureSchemaExists();
}
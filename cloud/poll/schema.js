const { ExtendedSchema, DateSchemaField, StringSchemaField } = require("../lib/extended_schema");

const properties = {
    className: 'poll',
    start_at: 'start_at',
    end_at: 'end_at',
    title: 'title'
}

exports.properties = properties;

exports.schema = new ExtendedSchema({
    name: properties.className,
    fields: [
        new DateSchemaField({ name: properties.date, acceptPast: false, mandatory: true, indexed: true }),
        new DateSchemaField({ name: properties.end_at, acceptPast: false, mandatory: true, indexed: true }),
        new StringSchemaField({name: properties.name, mandatory:true}),
    ]
});
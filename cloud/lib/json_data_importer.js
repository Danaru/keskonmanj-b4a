const fs = require('fs');

exports.import = async function({extendedSchema, jsonFilePath, keyField}) {
    console.log("Importing "+jsonFilePath+" file on collection "+extendedSchema.name+" ...");
    const rawdata = fs.readFileSync(jsonFilePath);
    const data = JSON.parse(rawdata);
    for(const element of data){
        var record = await existingRecord(extendedSchema.name, keyField, element[keyField]);
        if(!record){
            let parseObject = new Parse.Object(extendedSchema.name);
            extendedSchema.fields.forEach((field) => 
                parseObject.set(field.name, element[field.name])
            );
            await parseObject.save(null, {useMasterKey: true});
        }
        else {
            extendedSchema.fields.forEach((field) => 
                record.set(field.name, element[field.name])
            );
            await record.save(null, {useMasterKey: true});
        }
    }
    console.log("Import of "+jsonFilePath+" done...")
}
    
async function existingRecord(schemaName, keyField, expectedValue) {
    var query = new Parse.Query(schemaName).equalTo(keyField, expectedValue).limit(1);
    const result = await query.find({useMasterKey: true});
    if(result.length < 1){
        return;
    }
    return result[0]; 
}

const util = require('util');
exports.ExtendedSchema = class {
    constructor({
        name,
        systemFieldsConsts = ["objectId", "createdAt", "updatedAt", "ACL"],
        fields = [],
        clp = null,
        beforeFind = undefined,
        beforeCreate = async function (_) { },
        beforeUpdate = async function (_) { },
        afterCreate = async function (_) { },
        afterUpdate = async function (_) { },
        beforeDelete = async function (_) { },
        afterDelete = async function (_) { },
    }) {
        this._name = name;
        this._systemFieldsConsts = systemFieldsConsts;
        this._clp = clp;
        this._fields = fields;
        this._beforeFind = beforeFind;
        this._beforeCreate = beforeCreate;
        this._beforeUpdate = beforeUpdate;
        this._afterCreate = afterCreate;
        this._afterUpdate = afterUpdate;
        this._beforeDelete = beforeDelete;
        this._afterDelete = afterDelete;
    }


    get name() {
        return this._name;
    }

    get fields() {
        return this._fields;
    }

    get clp() {
        return this._clp;
    }

    async ensureExists() {
        var persistedSchema = new Parse.Schema(this._name);
        var updatedSchema = new Parse.Schema(this._name);
        try {
            persistedSchema = await persistedSchema.get();
        } catch (ex) {
            console.log("Creating " + this._name + " class...")
            persistedSchema = await persistedSchema.save();
        }

        for (const extendedField of this._fields) {
            this.ensureFieldExists(persistedSchema, updatedSchema, extendedField);
            this.applyIndex(persistedSchema, updatedSchema, extendedField);
        }
        this.deleteFields(persistedSchema, updatedSchema);

        updatedSchema.setCLP(this._clp);
        await updatedSchema.update();
    }

    ensureFieldExists(persistedSchema, updatedSchema, extendedField) {
        if (persistedSchema.fields[extendedField.name] === undefined) {
            console.log("Ajout du champ : " + this._name + "[" + extendedField.name + "]");
            extendedField.addField(updatedSchema);
        }
    }

    deleteFields(persistedSchema, updatedSchema) {
        for (const field in persistedSchema.fields) {
            if (this._systemFieldsConsts.includes(field)) {
                continue;
            }
            if (!this._fields.some((e) => e.ownsField(field))) {
                console.log("Suppression du champ : " + this._name + "[" + field + "]");
                updatedSchema.deleteField(field);
            }
        }
    }

    applyIndex(persistedSchema, updatedSchema, extendedField) {
        const indexName = extendedField.name + "Index";
        const alreadyIndexed = (persistedSchema.indexes && persistedSchema.indexes[indexName]);
        if (extendedField.indexed && alreadyIndexed) {
            console.log("Champ " + this._name + "[" + extendedField.name + "] indexé : OK");
            return;
        }
        if (!extendedField.indexed && !alreadyIndexed) {
            console.log("Champ " + this._name + "[" + extendedField.name + "] non indexé : OK");
            return;
        }

        if (extendedField.indexed) {
            console.log("Indexation du champ " + this._name + "[" + extendedField.name + "]");
            const index = {};
            index[extendedField.name] = 1;
            updatedSchema.addIndex(indexName, index);
            return;
        }

        if (!extendedField.indexed) {
            console.log("Suppression d'index sur le champ " + this._name + "[" + extendedField.name + "]");
            updatedSchema.deleteIndex(indexName);
            return;
        }
    }

    async applyTriggers() {
        this._applyBeforeFindTrigger();

        this._applyBeforeSaveTrigger();
        this._applyAfterSaveTrigger();

        this._applyBeforeDeleteTrigger();
        this._applyAfterDeleteTrigger();

    }

    _isCreating(request) { return request.original === undefined; }

    _applyBeforeFindTrigger() {
        if (this._beforeFind === undefined) return;
        
        Parse.Cloud.beforeFind(this._name, this._beforeFind);
    }

    _applyBeforeSaveTrigger() {
        Parse.Cloud.beforeSave(this._name, async (request) => {
            if (this._isCreating(request)) {

                for (var field of this._fields) {
                    await field.beforeCreate(request);
                }
                await this._beforeCreate(request);
            } else {

                for (var field of this._fields) {
                    await field.beforeUpdate(request);
                }
                await this._beforeUpdate(request);
            }
        });
    }

    _applyAfterSaveTrigger() {
        Parse.Cloud.afterSave(this._name, async (request) => {
            if (this._isCreating(request)) {
                for (var field of this._fields) {
                    await field.afterCreate(request)
                }
                await this._afterCreate(request);
            } else {
                for (var field of this._fields) {
                    await field.afterUpdate(request)
                }
                await this._afterUpdate(request);
            }
        });
    }

    _applyBeforeDeleteTrigger() {
        Parse.Cloud.beforeDelete(this._name, async (request) => {
            await this._beforeDelete(request);
            for (var field of this._fields) {
                await field.beforeDelete(request)
            }
        });
    }

    _applyAfterDeleteTrigger() {
        Parse.Cloud.afterDelete(this._name, async (request) => {
            await this._afterDelete(request);
            for (var field of this._fields) {
                await field.afterDelete(request)
            }
        });
    }
}

class ExtendedSchemaField {
    constructor({ name, readOnly = false, final = false, defaultValue, mandatory = false, indexed, unique = false, options } = {}) {
        this.name = name;
        this.options = options;
        this.readOnly = readOnly;
        this.final = final;
        this.defaultValue = defaultValue;
        this.mandatory = mandatory;
        this.indexed = indexed;
        this.unique = unique;
    }

    ownsField(fieldName) {
        return this.name === fieldName;
    }

    async beforeCreate(request) {
        this._ensureReadOnlyConstraint(request);
        this._applyDefaultValue(request);
        await this._ensureUniqueBeforeCreate(request);
        this._applyMandatoryConstraint(request);
    }


    async beforeUpdate(request) {
        this._ensureReadOnlyConstraint(request);
        this._applyMandatoryConstraint(request);
        await this._ensureUniqueBeforeUpdate(request);
        this._applyFinalConstraint(request);
    }

    isEqualTo(object1, object2) {
        return object1.get(this.name) == object2.get(this.name);
    }

    _applyFinalConstraint(request) {
        if (request.master) {
            return;
        }
        if (!this.final) {
            return;
        }
        if (request.original !== undefined && request.original.get(this.name) === undefined) {
            return;
        }
        if (request.object.get(this.name) === undefined) {
            return;
        }
        if (!this.isEqualTo(request.object, request.original)) {
            throw this.name + " is final and cannot be re-assigned."
        }
    }

    _applyMandatoryConstraint(request) {
        if (request.master) {
            return;
        }
        if (!this.mandatory) {
            return;
        }

        if (!request.object.get(this.name) && (request.original === undefined || !request.original.get(this.name))) {
            throw this.name + " is mandatory.";
        }
    }

    _ensureReadOnlyConstraint(request) {
        if (request.master) {
            return;
        }

        if (request.original !== undefined && this.isEqualTo(request.object, request.original)) {
            return;
        }

        if (this.readOnly && request.object.get(this.name)) {
            throw this.name + " is read only, you can't update its value.";
        }
    }

    _applyDefaultValue(request) {
        if (this.defaultValue === undefined || request.object.get(this.name) !== undefined) {
            return;
        }

        request.object.set(this.name, this.defaultValue);
    }

    async _ensureUniqueBeforeCreate(request) {
        await this._ensureUnique(request.object.className, request.object.get(this.name));

    }

    async _ensureUniqueBeforeUpdate(request) {
        var objectValue = request.object.get(this.name);
        var originalValue = request.original.get(this.name);
        if (originalValue === objectValue) return;
        await this._ensureUnique(request.object.className, objectValue)
    }

    async _ensureUnique(className, value) {
        if (!this.unique) return;
        if(value === undefined) return;
        
        var query = new Parse.Query(className);
        query.equalTo(this.name, value);
        query.limit(1);
        const exist = await query.count();
        if (exist > 0) {
            throw this.name + " is unique, a record with this value already exist.";
        }
    }

    async afterCreate(request) { }
    async afterUpdate(request) { }
    async beforeDelete(request) { }
    async afterDelete(request) { }

    addField(schema) { }
}

exports.ObjectSchemaField = class extends ExtendedSchemaField {
    addField(schema){
        schema.addObject(this.name, this.options);
    }
}

exports.BoolSchemaField = class extends ExtendedSchemaField {
    addField(schema){
        schema.addBoolean(this.name, this.options);
    }
}

exports.StringSchemaField = class extends ExtendedSchemaField {
    addField(schema) {
        schema.addString(this.name, this.options);
    }
}

exports.ArraySchemaField = class extends ExtendedSchemaField {
    addField(schema) {
        schema.addArray(this.name, this.options);
    }
}

class NumberSchemaField extends ExtendedSchemaField {
    addField(schema) {
        schema.addNumber(this.name, this.options);
    }
}
exports.NumberSchemaField = NumberSchemaField;


exports.NumberRangeSchemaField = class extends ExtendedSchemaField {
    constructor({ readOnly, mandatory, final, minName, maxName, defaultMinValue, defaultMaxValue, minValue, minOptions, maxOptions, indexed } = {}) {
        super({ name: minName });
        this._minField = new NumberSchemaField({ name: minName, readOnly: readOnly, final: final, defaultValue: defaultMinValue, mandatory: mandatory, indexed: indexed, options: minOptions });
        this._maxField = new NumberSchemaField({ name: maxName, readOnly: readOnly, final: final, defaultValue: defaultMaxValue, mandatory: mandatory, indexed: indexed, options: maxOptions });
        this._minValue = minValue;
    }

    addField(schema) {
        this._minField.addField(schema);
        this._maxField.addField(schema);
    }

    ownsField(fieldName) {
        return this._minField.name === fieldName || this._maxField.name === fieldName;
    }

    async beforeCreate(request) {
        await super.beforeCreate(request);

        await this._minField.beforeCreate(request);
        await this._maxField.beforeCreate(request);
        this._ensureMinValue(request);
        this._ensureRangeIsValid(request);
    }

    async beforeUpdate(request) {
        await super.beforeUpdate(request);
        
        await this._minField.beforeUpdate(request);
        await this._maxField.beforeUpdate(request);
        this._ensureMinValue(request);
        this._ensureRangeIsValid(request);
    }

    _ensureRangeIsValid(request) {
        const minValue = request.object.get(this._minField.name);
        const maxValue = request.object.get(this._maxField.name);

        if (minValue > maxValue) {
            throw this._minField.name + " should be inferior to " + this._maxField.name + ".";
        }
    }

    _ensureMinValue(request) {

        if (this._minValue === undefined) {
            return;
        }

        if (request.object.get(this._minField.name) < this._minValue) {
            throw this._minField.name + " should be superior or equal to " + this._minValue + ".";
        }
    }
}

exports.DateSchemaField = class extends ExtendedSchemaField {
    constructor({ name, readOnly, defaultValue, final, acceptPast = true, indexed, mandatory, options } = {}) {
        super({ name: name, readOnly: readOnly, defaultValue: defaultValue, indexed: indexed, final: final, mandatory: mandatory, options: options });
        this._acceptPast = acceptPast;
    }

    addField(schema) {
        schema.addDate(this.name, this.options);
    }

    async beforeCreate(request) {
        await super.beforeCreate(request);

        this._ensureAcceptPastConstraint(request);
    }

    async beforeUpdate(request) {
        await super.beforeUpdate(request);

        this._ensureAcceptPastConstraint(request);
    }

    _ensureAcceptPastConstraint(request) {
        if (this._acceptPast || request.master) {
            return;
        }
        const date = request.object.get(this.name);
        const currentDate = new Date();
        if (date <= currentDate) {
            throw this.name + " should be in future.";
        }
    }
}

exports.GeoPointSchemaField = class extends ExtendedSchemaField {
    addField(schema) {
        schema.addGeoPoint(this.name, this.options);
    }
}

exports.PointerSchemaField = class extends ExtendedSchemaField {
    constructor({ name, readOnly, defaultValue, final, pointerClassName, mandatory, indexed, mustExist = false, options } = {}) {
        super({ name: name, readOnly: readOnly, defaultValue: defaultValue, final: final, indexed: indexed, mandatory: mandatory, options: options });
        this._pointerClassName = pointerClassName;
        this._mustExist = mustExist;
    }

    addField(schema) {
        schema.addPointer(this.name, this._pointerClassName, this.options);
    }

    async beforeCreate(request) {
        await super.beforeCreate(request);
        if (!await this._pointedObjectExists(request)) {
            throw "Pointed object " + this._pointerClassName + " : " + this._pointedObjectId(request) + " does not exist.";
        }
    }

    _pointedObjectId(request) {
        const field = request.object.get(this.name);
        if (field) {
            return field.id;
        }
        return;
    }

    async _pointedObjectExists(request) {
        const pointerId = this._pointedObjectId(request);
        if (pointerId === undefined) {
            return true;
        }
        const query = new Parse.Query(this._pointerClassName);
        query.equalTo("objectId", pointerId);
        query.limit(1);

        return (await query.count({ useMasterKey: true })) === 1;
    }

    isEqualTo(requestObject, originalObject) {
        const objectValue = requestObject.get(this.name);
        const originalValue = originalObject.get(this.name);
        if (objectValue) {
            return objectValue.equals(originalValue);
        }
        return objectValue == originalValue;
    }
}

exports.FileSchemaField = class extends ExtendedSchemaField {
    constructor({ name, readOnly, defaultValue, cascade = true, mandatory, options } = {}) {
        super({ name: name, readOnly: readOnly, defaultValue: defaultValue, mandatory: mandatory, options: options });
        this._cascade = cascade;
    }

    addField(schema) {
        schema.addFile(this.name, this.options);
    }

    async afterUpdate(request) {
        await super.afterUpdate(request);
        this._purgeUnusedFile(
            this._getParseFile(request.object),
            this._getParseFile(request.original)
        );
    }

    async afterDelete(request) {
        await super.afterDelete(request);
        this._purgeUnusedFile(
            undefined,
            this._getParseFile(request.object)
        );
    }

    async _purgeUnusedFile(newFile, oldFile) {
        const parseFileToDelete = this._shouldDeleteFile(newFile, oldFile);
        if (parseFileToDelete !== undefined) {
            console.log("Purge du fichier " + JSON.stringify(parseFileToDelete));
            await parseFileToDelete.destroy();
        }
    }

    _shouldDeleteFile(newFile, oldFile) {
        if (!this._cascade) {
            return;
        }

        if (this._getParseFileName(newFile) !== this._getParseFileName(oldFile)) {
            return oldFile;
        }
    }

    _getParseFile(object) {
        if (object === undefined) {
            return;
        }
        return object.get(this.name);
    }


    _getParseFileName(parseFile) {
        if (parseFile === undefined) {
            return;
        }
        return parseFile.name();
    }
}

exports.RelationSchemaField = class extends ExtendedSchemaField {
    constructor({ name, readOnly, indexed, targetClassName, options } = {}) {
        super({ name: name, readOnly: readOnly, indexed: indexed, options: options });
        this._targetClassName = targetClassName;
    }

    addField(schema) {
        schema.addRelation(this.name, this._targetClassName);
    }

    isEqualTo(requestObject, originalObject) {
        const objectValue = requestObject.get(this.name);
        const originalValue = originalObject.get(this.name);
        if (objectValue.key == originalValue.key && objectValue.targetClassName == originalValue.targetClassName) {
            return true;
        }
        return false;
    }
}